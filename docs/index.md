# Open Hardware Day

<center>
![Logo OSHW](assets/logo.png){width="300"}
</center>

Bem-vindo ao Open Hardware Day, um evento que se propõe a explorar as amplas oportunidades no desenvolvimento de hardware aberto. O evento contará com diversas palestras abrangendo uma variedade de temas.

O evento ocorrerá na Universidade Estadual de Campinas no dia 24 de maio, e a participação é totalmente gratuita. Mais informações estão disponíveis na aba [sobre](about.md).

## Inscrição

Se desejar receber um certificado de participação no Open Hardware Day, pedimos que preencha [este formulário](https://forms.gle/ZEBdjKeqmeBsTfKM8). Para participantes virtuais, lamentamos informar que não será possível emitir certificados devido à impossibilidade de confirmar a presença. No entanto, enfatizamos que a participação no Open Hardware Day está aberta a todos, e mesmo aqueles que optarem por não preencher o formulário são bem-vindos a participar normalmente.

## Por que participar?

- **Networking**: Conecte-se com pessoas interessadas na área, ampliando sua rede de contatos na comunidade.

- **Atualização sobre novas tecnologias e técnicas**: Conheça novas ferramentas, tecnologias e técnicas no desenvolvimento de hardware.

- **Contribua para estimular o desenvolvimento de hardware no Brasil**: Ajude a despertar o interesse pelo desenvolvimento e fabricação de hardware no Brasil.

Não perca a oportunidade de fazer parte do Open Source Hardware Day - onde a inovação e a colaboração no mundo do hardware aberto estão em destaque. Junte-se a nós para uma jornada emocionante rumo ao futuro do desenvolvimento de hardware!
