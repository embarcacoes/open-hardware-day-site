# Chamada para Palestras

O evento contará com uma variedade de palestras, cada uma com duração máxima de uma hora e meia, incluindo o tempo destinado às perguntas.

Aqueles interessados em apresentar uma palestra devem preencher o formulário disponível neste [link](https://forms.gle/NvCAh62gPunaqLCK7), fornecendo informações sobre o tema da apresentação e os recursos necessários para sua realização.

Devido às restrições de horário, caso o número de propostas de palestras ultrapasse a capacidade de realização em um único dia, algumas propostas podem ser recusadas e/ou o evento pode ser estendido para um segundo dia.

O evento será conduzido em língua portuguesa, sendo preferíveis palestras nesse idioma, mas apresentações em outros idiomas também serão consideradas.

É importante observar que o conteúdo da apresentação não deve ser proprietário e/ou conter informações confidenciais.
