# Patrocinadores

Interessado em apoiar o Open Hardware Day? Entre em contato conosco pelo e-mail: [embarcacoes.unicamp@gmail.com](mailto:embarcacoes.unicamp@gmail.com)

## Benefícios de ser um patrocinador

- **Interação contínua com a comunidade durante o evento**
- **Aprimoramento da reputação da sua marca ao se posicionar ao lado de iniciativas Open Source**
- **Exibição dos símbolos da sua marca ao longo do evento**
- **Networking com indivíduos engajados no desenvolvimento de hardware**
- **Espaço para apresentar seus produtos e serviços, demonstrando o compromisso da sua marca com o Open Source**

## Reconhecimento

Ao ser um patrocinador, você terá:

- **Inclusão da sua logomarca no material de divulgação do evento e no website**
- **Divulgação da sua marca durante os Coffee Breaks**
- **Agradecimentos durante a sessão de abertura**
- **Oportunidade de apresentação durante a sessão de encerramento**

Destacamos a importância de sua marca em todos os aspectos do evento, proporcionando visibilidade significativa e uma oportunidade única para interagir com a comunidade e reforçar seu compromisso com o universo open source.
