# Sobre o evento

<center>
![Logo OSHW](assets/logo.png){width="300"}
</center>

## Proposta

O Open Hardware Day é um evento dedicado a evidenciar as vastas oportunidades no desenvolvimento de hardware de forma aberta.

## Detalhes do evento:

- **Data**: 24/05/2025
- **Local**: Auditório do Instituto de Computação da Universidade Estadual de Campinas ([UNICAMP](https://www.unicamp.br))
- **Transmissão**: [Youtube](https://youtube.com/@embarcacoes.unicamp)

## Realização

O Open Hardware Day é realizado em colaboração entre o [Grupo de Estudos e Desenvolvimento de Sistemas Embarcados (Embarcações)](https://embarcacoes.ic.unicamp.br/), e o Capitulo estudantil CS - [IEEE](https://www.instagram.com/ieeeunicamp/).

<center>
![Logo Embarcações](assets/embarcacoes.png){width="300"}
![Logo IEEE](assets/ieee-color-logo-r.png){width="270"} 
</center>

### Equipe:

**Coordenador:**

- Julio Nunes Avelar - [julio.avelar@students.ic.unicamp.br](mailto:julio.avelar@students.ic.unicamp.br)

**Desenvolvedor do Site:**

- Julio Nunes Avelar - [julio.avelar@students.ic.unicamp.br](mailto:julio.avelar@students.ic.unicamp.br)

**Design:**

- Julio Nunes Avelar - [julio.avelar@students.ic.unicamp.br](mailto:julio.avelar@students.ic.unicamp.br)

**Inflaestrutura**

- Julio Nunes Avelar - [julio.avelar@students.ic.unicamp.br](mailto:julio.avelar@students.ic.unicamp.br)

## Apoio

- Faculdade de engenharia eletrica e Computação ([FEEC](https://www.fee.unicamp.br))
- Instituto de Computação ([IC](https://ic.unicamp.br))

<br>
<center>
![Logo FEEC](assets/logo-feec.png){width="300"}
![Logo IC](assets/ic-circle.png){width="320"} 
</center>
