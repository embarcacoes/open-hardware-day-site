# Divulgação

## Cartaz

## Logos

[OSH](assets/logo.png){:download="logo.png"}

[Embarcações](assets/embarcacoes.png){:download="embarcacoes.png"}

[IEEE](assets/ieee.png){:download="ieee.png"}

## Imagens para divulgação em redes sociais

[Chamada de trabalhos](assets/chamada_de_trabalhos.png){:download="chamada_de_trabalhos.png"}

## Links

[Grupo no telegram](https://t.me/+zMClTkNKosExZWZh)
