# Cronograma

## Datas do evento

| Data       | Atividade                                 |
| ---------- | ----------------------------------------- |
| 24/02/2024 | Início da chamada para palestrantes       |
| 13/04/2024 | Encerramento da chamada para palestrantes |
| 05/04/2024 | Divulgação do cronograma de Palestras     |
| 24/05/2024 | Realização do evento                      |

## Palestras

| Horario | Palestra                                                                                                                                        | Autor                |
| ------- | ----------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
